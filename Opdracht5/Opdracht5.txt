Studentnummer:
Naam:

Verschilt de Terminal in Visual Studio Code met Git Bash?
	# Het is eigenlijk het zelfde als de terminal van VSC

Waar worden Branches vaak voor gebruikt?
	# werken op features/bugfixes zonder het gelijk in de master te moeten doen
Hoe vaak ben je in Opdracht 5A van Branch gewisseld?
	# 2 keer

Vul de volgende commando's aan:
 -Checken op welke branch je aan het werken bent:
	# git branch
 -Nieuwe Branch aanmaken
	# git branch [Naam]
 -Van Branch wisselen
	# git checkout [Naam]
 -Branch verwijderen
	# git branch -d [Naam]